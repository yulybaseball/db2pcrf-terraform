profile_name = "3P-LowerInt"
instance_size = "t2.micro"
image_id = "ami-03ac83f732543d81e"
subnet_id = "subnet-0c3fdb3c8977d7f42"
security_group_names = ["sg-0e15dca36201590d0", "sg-0c292918fee14bad3"]
ssh_key = "3p-integration-key"
tags = {
  "Name" : "PCRF2DB_0",
  "owner" : "ypaz"
  "project" : "Cloud Migration - Application Integration"
}
