terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

provider "aws" {
  region = "us-east-1"
  profile = var.profile_name
}

resource "aws_instance" "PCRF2DB_0" {
  instance_type = var.instance_size
  ami = var.image_id
  subnet_id = var.subnet_id
  vpc_security_group_ids = var.security_group_names
  key_name = var.ssh_key
  user_data = file("init-pcrf2db_0.sh")
  tags = var.tags
}
