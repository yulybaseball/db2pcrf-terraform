variable "region" {
  default = "us-east-1"
}

variable "profile_name" {
  type = string
}

variable "instance_size" {
  type = string
}

variable "image_id" {
  type = string
}

variable "subnet_id" {
  type = string
}

variable "security_group_names" {
  type = list(string)
}

variable "ssh_key" {
  type = string
}

variable "tags" {
  type = map(string)
}

